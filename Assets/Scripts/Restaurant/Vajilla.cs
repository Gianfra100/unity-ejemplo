﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurante
{
    abstract class Vajilla
    {
        protected int cantidadInicial = 0;
        protected int cantidadActual = 0;

        public Vajilla (int cantidadInicial, int cantidadActual)
        {
            this.cantidadInicial = cantidadInicial;
        }

        protected Vajilla(int cantidadInicial)
        {
            this.cantidadInicial = cantidadInicial;
        }

        public virtual string GetInfo()
        {
                return "[\n cantidad: " + cantidadInicial + "\n]";
        }
    }
}
