﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    static int Platos = 50;
    static int Vasos = 60;
    static int Copas = 40;
    static int Tazas = 30;
    static int Jarras = 20;


    string str;

    string next;

    // Start is called before the first frame update
    void Start()
    {
        bool loop = true;

        while (loop)
        {
            CrearVajilla();
        }
    }

    void CrearVajilla()
    {
        Debug.Log("Stock actual");
        Debug.Log("Platos: " + Platos);
        Debug.Log("Vasos: " + Vasos);
        Debug.Log("Copas: " + Copas);
        Debug.Log("Tazas: " + Tazas);
        Debug.Log("Jarras: " + Jarras);
        Debug.Log("===================================");
        Debug.Log("Elija cual agregar");
        Debug.Log("   1 - Plato");
        Debug.Log("   2 - Vaso");
        Debug.Log("   3 - Copa");
        Debug.Log("   4 - Taza");
        Debug.Log("   5 - Jarra");

        Debug.Log("Cuántos desea agregar?");

        int agregar = 30;

        switch (str)
        {
            case "1":
                Platos = Platos + agregar;
                break;
            case "2":
                Vasos = Vasos + agregar;
                break;
            case "3":
                Copas = Copas + agregar;
                break;
            case "4":
                Tazas = Tazas + agregar;
                break;
            case "5":
                Jarras = Jarras + agregar;
                break;
        }

        Debug.Log("Stock actual");
        Debug.Log("Platos: " + Platos);
        Debug.Log("Vasos: " + Vasos);
        Debug.Log("Copas: " + Copas);
        Debug.Log("Tazas: " + Tazas);
        Debug.Log("Jarras: " + Jarras);

        Debug.Log("Agregar algo más? Y/N");
    }
}
