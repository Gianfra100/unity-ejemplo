﻿using System;

namespace Restaurante
{
    class Program

    {
        static int Platos = 50;
        static int Vasos = 60;
        static int Copas = 40;
        static int Tazas = 30;
        static int Jarras = 20;


        private static string str;

        static string next;

        static void Main(string[] args)
        {
            bool loop = true;

            while (loop) 
            {
                Console.Clear();
                CrearVajilla();

                if (!(next.ToUpper() == "Y"))
                    loop = false;
            }
        }

        static void CrearVajilla()
        {
            Vajilla vajilla = null;
            Console.WriteLine("Stock actual");
            Console.WriteLine("Platos: " + Platos);
            Console.WriteLine("Vasos: " + Vasos);
            Console.WriteLine("Copas: " + Copas);
            Console.WriteLine("Tazas: " + Tazas);
            Console.WriteLine("Jarras: " + Jarras);
            Console.WriteLine("===================================");
            Console.WriteLine("Elija cual agregar");
            Console.WriteLine("   1 - Plato");
            Console.WriteLine("   2 - Vaso");
            Console.WriteLine("   3 - Copa");
            Console.WriteLine("   4 - Taza");
            Console.WriteLine("   5 - Jarra");

            Console.WriteLine("Cuántos desea agregar?");

            string agregar = Console.ReadLine();

            switch (str)
            {
                case "1":
                    Platos = Platos + Convert.ToInt32(agregar);
                    break;
                case "2":
                    Vasos = Vasos + Convert.ToInt32(agregar);
                    break;
                case "3":
                    Copas = Copas + Convert.ToInt32(agregar); 
                    break;
                case "4":
                    Tazas = Tazas + Convert.ToInt32(agregar); 
                    break;
                case "5":
                    Jarras = Jarras + Convert.ToInt32(agregar); 
                    break;
            }

            Console.WriteLine("Stock actual");
            Console.WriteLine("Platos: " + Platos); 
            Console.WriteLine("Vasos: " + Vasos);
            Console.WriteLine("Copas: " + Copas);
            Console.WriteLine("Tazas: " + Tazas);
            Console.WriteLine("Jarras: " + Jarras);

            Console.WriteLine("Agregar algo más? Y/N");
            next = Console.ReadLine();
        }
    }
}
